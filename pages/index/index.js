import { request } from "../../request/index.js";

Page({
  data: {
    //轮播图数组
    swiperList: [],
    cateList: [],
    floorList: []
  },
  onLoad: function(options){
    //发送异步请求 
    // wx-wx.request({
    //   url: 'https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata',
    //   success: (result) => {
    //     this.setData({
    //       swiperList: result.data.message
    //     })
    //     console.log(result.data.message);
    //   },
    // })
    this.getSwiperList();
    this.getCateList();
    this.getFloorList();
  },
  onReady: function(options){

  },
  onShow: function(){

  },
  onHide: function(){

  },
  onUnload: function(){

  },
  onPullDownRefresh: function(){

  },
  onReachBottom: function(){

  },
  onShareAppMessage: function(){

  },
  onPageScroll: function(){

  },
  onTabItemTao: function(item){

  },
  //获取轮播图数据
  getSwiperList(){
    request({url:"/home/swiperdata"}).then(result => {
      this.setData({
        swiperList: result.data.message
      })
    })
  },
  //获取分类数据
  getCateList(){
    request({url:"/home/catitems"}).then(result => {
      this.setData({
        cateList: result.data.message
      })
    })
  },
  //获取楼层数据
  getFloorList(){
    request({url:"/home/floordata"}).then(result => {
      this.setData({
        floorList: result.data.message
      })
    })
  }
});